import os
import torch
import argparse
import numpy as np
from torch import nn, optim
from torch.nn import functional as F
from torchvision.utils import save_image
from torchvision import datasets, transforms
from torch.optim.lr_scheduler import StepLR

# -----------------
#  Configuration
# -----------------

# parsing
parser = argparse.ArgumentParser(description="This is my implementation of CVAE")

# configuration for training
parser.add_argument('--batch_size', type=int, default=100, help='batch size')
parser.add_argument('--n_epochs', type=int, default=10, help='number of epochs')
parser.add_argument('--lr', type=int, default=1e-3, help='learning rate')
parser.add_argument('--z_dim', type=int, default=2, help='dimension of latent variable z')
parser.add_argument("--b1", type=float, default=0.9, help="adam: decay of first order momentum of gradient")
parser.add_argument("--b2", type=float, default=0.999, help="adam: decay of second order momentum of gradient")
parser.add_argument('--lrstep', type=int, default=1200, help='learning rate decay step')
parser.add_argument('--gamma', type=float, default=1, help='learning rate decay coefficient')
parser.add_argument('--middim1', type=int, default=512, help='inner dim of fc')
parser.add_argument('--middim2', type=int, default=256, help='inner dim of fc')

# configuration for validating
parser.add_argument("--only_validating", action='store_true',
                    help="Only validating or not(If True, you must have a pretrained model at m_path!!!)")
parser.add_argument("--m_path", type=str, default="pretrained_model/cvae.pth", help="save path of cvae model")

# configuration for testing
parser.add_argument("--d_path", type=str, default="pretrained_model/cvae_decoder.pth", help="save path of decoder")
parser.add_argument("--only_testing", action='store_true',
                    help="Only testing or not(If True, you must have a pretrained model at d_path, and"
                         "z_dim must equal to 1 or 2)")
parser.add_argument('--sample_step', type=int, default=20, help='sample step of z')
parser.add_argument('--gen_label', type=int, default=1, help='condition to generate images')

# get args from parser
args = parser.parse_args()
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
batch_size = args.batch_size
z_dim = args.z_dim
n_epochs = args.n_epochs
lr = args.lr
b1 = args.b1
b2 = args.b2
print_interval = 200000/batch_size
sample_interval = 200000/batch_size
valid_print_interval = 8000/batch_size
valid_sample_interval = 8000/batch_size
m_path = args.m_path
d_path = args.d_path
sample_step = args.sample_step

# -----------------
#  Definition
# -----------------


# calculate loss
def cal_loss(gen_imgs, imgs, mean, var):
    bce = bceloss(gen_imgs, imgs.view(-1, 784))
    kld = 0.5 * torch.sum(mean.pow(2) + var.exp() - var - 1)
    total_loss = bce + kld
    return total_loss, bce, kld


def to_one_hot(labels: torch.Tensor, num_class: int):
    y = torch.zeros(labels.shape[0], num_class)
    for i, label in enumerate(labels):
        y[i, label] = 1
    return y


# function to sample z for testing
def sample_z():
    if z_dim == 2:
        x = torch.linspace(start=-5, end=5, steps=sample_step)
        y = torch.linspace(start=-5, end=5, steps=sample_step)
        x1, y1 = torch.meshgrid((x, y), indexing = 'ij')
        z = torch.stack((x1, y1), 2).view(-1, 2)

    elif z_dim == 1:
        z = torch.linspace(start=-5, end=5, steps=sample_step*sample_step).view(-1, 1)

    return z


# Encoder
class Encoder(nn.Module):
    def __init__(self):
        super(Encoder, self).__init__()
        self.fc1 = nn.Linear(794, args.middim1)
        self.fc2 = nn.Linear(args.middim1, args.middim2)
        self.fcm = nn.Linear(args.middim2, z_dim)
        self.fcvar = nn.Linear(args.middim2, z_dim)

    def forward(self, x, y):
        h = F.relu(self.fc1(torch.cat([x, y], dim=1)))  # concat features and labels
        h = F.relu(self.fc2(h))
        mean, var = self.fcm(h), self.fcvar(h)
        return mean, var


# Reparameterization
class Reparameterization(nn.Module):
    def __init__(self):
        super(Reparameterization, self).__init__()

    def forward(self, mean, var):
        s = torch.exp(0.5 * var)
        e = torch.randn_like(s)
        z = mean + e * s
        return z

# Decoder
class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()
        self.fc3 = nn.Linear(z_dim+10, args.middim2)
        self.fc4 = nn.Linear(args.middim2, args.middim1)
        self.fc5 = nn.Linear(args.middim1, 784)

    def forward(self, z, y):
        x = F.relu(self.fc3(torch.cat([z, y], dim=1)))  # concat latents and labels
        x = F.relu(self.fc4(x))
        x= self.fc5(x)
        x = torch.sigmoid(x)
        return x


# CVAE model
class CVAE(nn.Module):
    def __init__(self):
        super(CVAE, self).__init__()
        self.encoder = Encoder()
        self.reparameterization = Reparameterization()
        self.decoder = Decoder()

    def forward(self, x, y):
        x = x.view(-1, 784)

        # encoder
        mean, var = self.encoder(x, y)

        # reparameterization
        z = self.reparameterization(mean, var)

        # decoder
        x = self.decoder(z, y)

        return x, mean, var


# loss function  use "mean" may have a bad effect on gradients
bceloss = torch.nn.BCELoss(reduction='sum')

# initialize model
model = CVAE().to(device)

# initialize optimizer, scheduler
optimizer = optim.Adam(model.parameters(), lr=lr, betas=(b1, b2))
scheduler = StepLR(optimizer=optimizer, step_size=args.lrstep, gamma=args.gamma)

if __name__ == '__main__':
    os.makedirs("images", exist_ok=True)
    os.makedirs("pretrained_model", exist_ok=True)

    # -----------------
    #  Dataloader
    # -----------------

    # Download MNIST Dataset
    train_dataset = datasets.MNIST(root='./data/', train=True, transform=transforms.ToTensor(), download=True)
    valid_dataset = datasets.MNIST(root='./data/', train=False, transform=transforms.ToTensor(), download=False)

    # MNist Data Loader
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True)
    valid_loader = torch.utils.data.DataLoader(dataset=valid_dataset, batch_size=batch_size, shuffle=False)

    # -----------------
    #  Training
    # -----------------

    # only validating or testing
    if args.only_validating:
        # load model
        model.load_state_dict(torch.load(m_path))

    elif args.only_testing:
        # load decoder
        decoder = Decoder().to(device)
        decoder.load_state_dict(torch.load(d_path))

    else:
        model.train()
        for epoch in range(n_epochs):
            for i, (imgs, label) in enumerate(train_loader):

                # zero grad
                optimizer.zero_grad()

                # get data and label
                imgs = imgs.to(device)
                label = to_one_hot(label.reshape(-1, 1), num_class=10).to(device)

                # forward
                gen_imgs, mean, var = model(imgs, label)

                # calculate loss
                total_loss, bce, kld = cal_loss(gen_imgs, imgs, mean, var)

                # update
                total_loss.backward()
                optimizer.step()
                scheduler.step()

                # print the loss
                if i % print_interval == 0:
                    print(
                        "Training [Epoch %d/%d] [Batch %d/%d] [lr: %f] [total loss: %f] [bce loss: %f] [kld loss: %f]"
                        % (epoch, n_epochs, i, len(train_loader), optimizer.param_groups[0]['lr'],
                           total_loss.item() / batch_size, bce.item() / batch_size, kld.item() / batch_size)
                    )

                # sample images
                batches_done = epoch * len(train_loader) + i
                if batches_done % sample_interval == 0:
                    nelem = gen_imgs.size(0)
                    save_image(gen_imgs.view(nelem, 1, 28, 28), "images/cvae_train_%d.png" % batches_done, nrow=10, normalize=True)

        # save model
        torch.save(model.state_dict(), m_path)
        torch.save(model.decoder.state_dict(), d_path)
        # load model
        decoder = Decoder().to(device)
        decoder.load_state_dict(torch.load(d_path))

    # -----------------
    #  Validation
    # -----------------

    if not args.only_testing:
        print("\nValidating start!")

        model.eval()
        with torch.no_grad():
            for i, (imgs, label) in enumerate(valid_loader):

                # get data and label
                imgs = imgs.to(device)
                label = to_one_hot(label.reshape(-1, 1), num_class=10).to(device)

                # forward
                gen_imgs, mean, var = model(imgs, label)

                # calculate loss
                total_loss, bce, kld = cal_loss(gen_imgs, imgs, mean, var)

                # print the loss
                if i % valid_print_interval == 0:
                    print(
                        "Validating [Batch %d/%d] [total loss: %f] [bce loss: %f] [kld loss: %f]"
                        % (i, len(valid_loader), total_loss.item() / batch_size, bce.item() / batch_size,
                           kld.item() / batch_size)
                    )

                # sample images
                batches_done = i
                if batches_done % valid_sample_interval == 0:
                    nelem = gen_imgs.size(0)
                    save_image(gen_imgs.view(nelem, 1, 28, 28), "images/cvae_valid_%d.png" % batches_done, nrow=10, normalize=True)

        print("Validating done!")

    # -----------------
    #  Testing
    # -----------------

    # if z_dim > 2 , no testing!
    if z_dim > 2:
        no_testing = True
    else:
        no_testing = False

    if not args.only_validating and not no_testing:
        print("\nTesting start!")

        decoder.eval()

        # sample z
        z = sample_z()
        nelem = sample_step * sample_step

        # define gen_label
        labels = torch.full(size=(nelem, 1), fill_value=args.gen_label, dtype=torch.int64)
        labels = to_one_hot(labels, num_class=10)

        # generate images
        with torch.no_grad():
            gen_imgs = decoder(z.to(device), labels.to(device))

        # save images
        save_image(gen_imgs.view(nelem, 1, 28, 28), f"images/cvae_test_label{args.gen_label}_zdim{z_dim}.png", nrow=sample_step, normalize=True)

        print("Testing done!")
