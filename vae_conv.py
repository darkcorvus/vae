import os
import torch
import argparse
import numpy as np
from torch import nn, optim
from torch.nn import functional as F
from torchvision.utils import save_image
from torchvision import datasets, transforms
from torch.optim.lr_scheduler import StepLR

# -----------------
#  Configuration
# -----------------

# parsing
parser = argparse.ArgumentParser(description="This is my implementation of VAE")

# configuration for training
parser.add_argument('--batch_size', type=int, default=128, help='batch size')
parser.add_argument('--n_epochs', type=int, default=10, help='number of epochs')
parser.add_argument('--lr', type=int, default=1e-3, help='learning rate')
parser.add_argument('--z_dim', type=int, default=2, help='dimension of latent variable z')
parser.add_argument("--b1", type=float, default=0.9, help="adam: decay of first order momentum of gradient")
parser.add_argument("--b2", type=float, default=0.999, help="adam: decay of second order momentum of gradient")
parser.add_argument('--lrstep', type=int, default=1200, help='learning rate decay step')
parser.add_argument('--gamma', type=float, default=0.9, help='learning rate decay coefficient')
parser.add_argument('--middim1', type=int, default=512, help='inner dim of fc')
parser.add_argument('--middim2', type=int, default=256, help='inner dim of fc')

# configuration for validating
parser.add_argument("--only_validating", type=bool, default=False,
                    help="Only validating or not(If True, you must have a pretrained model at m_path!!!)")
parser.add_argument("--m_path", type=str, default="pretrained_model/vae_conv.pth", help="save path of vae model")

# configuration for testing
parser.add_argument("--d_path", type=str, default="pretrained_model/vae_decoder_conv.pth", help="save path of decoder")
parser.add_argument("--only_testing", action='store_true',
                    help="Only testing or not(If True, you must have a pretrained model at d_path, and"
                         "z_dim must equal to 1 or 2)")
parser.add_argument('--sample_step', type=int, default=20, help='sample steps of z')

# get args from parser
args = parser.parse_args()
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
batch_size = args.batch_size
z_dim = args.z_dim
n_epochs = args.n_epochs
lr = args.lr
b1 = args.b1
b2 = args.b2
print_interval = 20000/batch_size
sample_interval = 20000/batch_size
valid_print_interval = 2000/batch_size
valid_sample_interval = 2000/batch_size
m_path = args.m_path
d_path = args.d_path
sample_step = args.sample_step

# -----------------
#  Definition
# -----------------


# calculate loss
def cal_loss(gen_imgs, imgs, mean, var):
    bce = bceloss(gen_imgs, imgs.view(-1, 784))
    kld = 0.5 * torch.sum(mean.pow(2) + var.exp() - var - 1)
    total_loss = bce + kld
    return total_loss, bce, kld


# function to sample z for testing
def sample_z():
    if z_dim == 2:
        x = torch.linspace(start=-5, end=5, steps=sample_step)
        y = torch.linspace(start=-5, end=5, steps=sample_step)
        x1, y1 = torch.meshgrid((x, y), indexing = 'ij')
        z = torch.stack((x1, y1), 2).view(-1, 2)

    elif z_dim == 1:
        z = torch.linspace(start=-5, end=5, steps=sample_step*sample_step).view(-1, 1)

    return z


# encoder
class EncoderConv(nn.Module):
    def __init__(self, z_size):
        super(EncoderConv, self).__init__()
        # encoder part
        self.conv_sequence = nn.Sequential(
            nn.Conv2d(1, 8, kernel_size=5, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(2),
            nn.Conv2d(8, 16, kernel_size=5, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(2),
            # nn.Conv2d(16,32,kernel_size=5,padding=2),
            # nn.ReLU(),
        )
        self.fc_sequence = nn.Sequential(
            nn.Linear(16 * 7 * 7, 128),
            nn.LeakyReLU(),
            nn.Linear(128, 64),
            nn.LeakyReLU(),
            # nn.Linear(128,64),
            # nn.LeakyReLU(),
        )
        self.fc_mean = nn.Linear(64, z_size)
        self.fc_std = nn.Linear(64, z_size)

    def forward(self, x):
        x = self.conv_sequence(x)
        x = x.flatten(-3)
        x = self.fc_sequence(x)
        mean = self.fc_mean(x)
        std = self.fc_std(x)
        return torch.clamp(mean, -5, 5), torch.clamp(std, -3, 3)


# decoder
class DecoderConv(nn.Module):
    def __init__(self, z_size):
        super(DecoderConv, self).__init__()
        # decoder part
        self.fc_seq = nn.Sequential(
            nn.Linear(z_size, 64),
            nn.LeakyReLU(),
            nn.Linear(64, 16 * 7 * 7),
            nn.LeakyReLU(),
        )
        self.reverse_conv = nn.Sequential(
            nn.ConvTranspose2d(16, 8, kernel_size=5, padding=2),
            nn.LeakyReLU(),
            nn.UpsamplingBilinear2d(14),
            nn.Conv2d(8, 8, kernel_size=5, padding=2),
            nn.ConvTranspose2d(8, 1, kernel_size=5, padding=2),
            nn.LeakyReLU(),
            nn.UpsamplingBilinear2d(28),
            nn.Conv2d(1, 1, kernel_size=5, padding=2),
        )
        self.output = nn.Sigmoid()

    def forward(self, z):
        x = self.fc_seq(z)
        B, F = x.shape
        x = x.reshape((B, 16, 7, 7))
        x = self.reverse_conv(x)  # [B,1,28,28]
        x = self.output(x)
        return x.view(-1, 784)


# vae
class VAE_Conv(nn.Module):
    def __init__(self):
        super(VAE_Conv, self).__init__()
        self.encoder = EncoderConv(z_dim)
        self.decoder = DecoderConv(z_dim)

    def sampling(self, mu, log_var):
        std = torch.exp(0.5 * log_var)
        eps = torch.randn_like(std)
        return eps.mul(std).add_(mu)

    def forward(self, x):
        if len(x.shape) == 3:
            x = x.unsqueeze(1)
        mean, std = self.encoder(x)
        sampled_z = self.sampling(mean, std)
        return self.decoder(sampled_z), mean, std


# loss function  use "mean" may have a bad effect on gradients
bceloss = torch.nn.BCELoss(reduction='sum')

# initialize model
model = VAE_Conv().to(device)

# initialize optimizer, scheduler
optimizer = optim.Adam(model.parameters(), lr=lr, betas=(b1, b2))
scheduler = StepLR(optimizer=optimizer, step_size=args.lrstep, gamma=args.gamma)

if __name__ == '__main__':
    os.makedirs("images", exist_ok=True)
    os.makedirs("pretrained_model", exist_ok=True)

    # -----------------
    #  Dataloader
    # -----------------

    # Download MNIST Dataset
    train_dataset = datasets.MNIST(root='./data/', train=True, transform=transforms.ToTensor(), download=True)
    valid_dataset = datasets.MNIST(root='./data/', train=False, transform=transforms.ToTensor(), download=False)

    # MNist Data Loader
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True)
    valid_loader = torch.utils.data.DataLoader(dataset=valid_dataset, batch_size=batch_size, shuffle=False)

    # -----------------
    #  Training
    # -----------------

    # only validating or testing
    if args.only_validating:
        # load model
        model.load_state_dict(torch.load(m_path))

    elif args.only_testing:
        # load decoder
        decoder = DecoderConv(z_dim).to(device)
        decoder.load_state_dict(torch.load(d_path))

    else:
        model.train()
        for epoch in range(n_epochs):
            for i, (imgs, label) in enumerate(train_loader):

                # zero grad
                optimizer.zero_grad()

                # get data
                imgs = imgs.to(device)

                # forward
                gen_imgs, mean, var = model(imgs)

                # calculate loss
                total_loss, bce, kld = cal_loss(gen_imgs, imgs, mean, var)

                # update
                total_loss.backward()
                optimizer.step()
                scheduler.step()

                # print the loss
                if i % print_interval == 0:
                    print(
                        "Training [Epoch %d/%d] [Batch %d/%d] [lr: %f] [total loss: %f] [bce loss: %f] [kld loss: %f]"
                        % (epoch, n_epochs, i, len(train_loader), optimizer.param_groups[0]['lr'],
                           total_loss.item()/batch_size, bce.item()/batch_size, kld.item()/batch_size)
                    )

                # sample images
                batches_done = epoch * len(train_loader) + i
                if batches_done % sample_interval == 0:
                    nelem = gen_imgs.size(0)
                    save_image(gen_imgs.view(nelem, 1, 28, 28), "images/vae_conv_train_%d.png" % batches_done, nrow=10, normalize=True)

        # save model
        torch.save(model.state_dict(), m_path)
        torch.save(model.decoder.state_dict(), d_path)
        # load model
        decoder = DecoderConv(z_dim).to(device)
        decoder.load_state_dict(torch.load(d_path))

    # -----------------
    #  Validation
    # -----------------

    if not args.only_testing:
        print("\nValidating start!")
        valid_loss = []

        model.eval()
        with torch.no_grad():
            for i, (imgs, label) in enumerate(valid_loader):

                # get data
                imgs = imgs.to(device)

                # forward
                gen_imgs, mean, var = model(imgs)

                # calculate loss
                total_loss, bce, kld = cal_loss(gen_imgs, imgs, mean, var)
                valid_loss.append(total_loss.item()/batch_size)

                # print the loss
                if i % valid_print_interval == 0:
                    print(
                        "Validating [Batch %d/%d] [total loss: %f] [bce loss: %f] [kld loss: %f]"
                        % (i, len(valid_loader), total_loss.item()/batch_size, bce.item()/batch_size, kld.item()/batch_size)
                    )

                # sample images
                batches_done = i
                if batches_done % valid_sample_interval == 0:
                    nelem = gen_imgs.size(0)
                    save_image(gen_imgs.view(nelem, 1, 28, 28), "images/vae_conv_valid_%d.png" % batches_done, nrow=10, normalize=True)

        print(f"Validating mean loss: {np.mean(valid_loss)}")
        print("Validating done!")

    # -----------------
    #  Testing
    # -----------------

    # if z_dim > 2 , no testing!
    if z_dim > 2:
        no_testing = True
    else:
        no_testing = False

    if not args.only_validating and not no_testing:
        print("\nTesting start!")

        decoder.eval()

        # sample z
        z = sample_z()
        nelem = sample_step*sample_step

        # generate images
        with torch.no_grad():
            gen_imgs = decoder(z.to(device))

        # save images
        save_image(gen_imgs.view(nelem, 1, 28, 28), f"images/vae_conv_test_zdim{z_dim}_lr{lr}_decay{args.gamma}.png", nrow=sample_step, normalize=True)

        print("Testing done!")
