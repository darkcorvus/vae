- 运行vae_fc，可以指定z_dim：

```shell
python vae.py --z_dim 2
```

- 可以指定only validate/test，但需要有pretrained_model，如：

```shell
python vae.py --only_testing
```

- 运行vae_conv：

```shell
python vae_conv.py
```

- 运行cvae，可以指定label：

```shell
python cvae.py --gen_label 2
```

- 更详细的参数请参考代码，或运行：

```shell
python cvae.py -h
```